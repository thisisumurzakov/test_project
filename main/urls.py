from django.urls import path, include

from .views import (
    ClientView, ClientDetailView,
    CarView, CarDetailView,
    OrderView, OrderDetailView,
    ExtendOrderView, ExtendOrderDetailView,
    OrdersCountView, CarStatisticsView,
    OrderStatisticsView, ClientStatisticsView
)

urlpatterns = [
    path('', include('account.urls')),
    path('client/', ClientView.as_view()),
    path('client/<int:pk>/', ClientDetailView.as_view()),
    path('car/', CarView.as_view()),
    path('car/<int:pk>/', CarDetailView.as_view()),
    path('order/', OrderView.as_view()),
    path('order/<int:pk>/', OrderDetailView.as_view()),
    path('extend_order/', ExtendOrderView.as_view()),
    path('extend_order/<int:pk>/', ExtendOrderDetailView.as_view()),
    path('count/orders/', OrdersCountView.as_view()),
    path('statistics/car/<int:pk>/', CarStatisticsView.as_view()),
    path('statistics/order/<int:pk>/', OrderStatisticsView.as_view()),
    path('statistics/client/<int:client>/<int:order>/', ClientStatisticsView.as_view()),
]