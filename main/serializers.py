from rest_framework import serializers

from .models import Client, Car, Order, Extended_order

class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        exclude = ('added_by',)


class CarSerializer(serializers.ModelSerializer):
    class Meta:
        model = Car
        exclude = ('added_by',)


class OrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Order
        exclude = ('added_by',)


class ExtendedOrderSerializer(serializers.ModelSerializer):
    from_datetime = serializers.DateTimeField(required=False)
    class Meta:
        model = Extended_order
        fields = '__all__'


class UpdateExtendedOrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Extended_order
        fields = ('to_datetime',)


class OrdersCountSerializer(serializers.Serializer):
    datetime = serializers.DateTimeField()