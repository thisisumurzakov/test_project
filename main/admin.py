from django.contrib import admin

from .models import Client, Car, Order, Extended_order


admin.site.register(Client)
admin.site.register(Car)
admin.site.register(Order)
admin.site.register(Extended_order)