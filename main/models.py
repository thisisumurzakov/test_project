from django.db import models
from django.contrib.auth.models import User


class Client(models.Model):
    passport_series = models.CharField(max_length=10, unique=True)
    firstname = models.CharField(max_length=100)
    lastname = models.CharField(max_length=100)
    phone = models.CharField(max_length=16, unique=True)
    added_by = models.ForeignKey(User, on_delete=models.CASCADE)


class Car(models.Model):
    brand = models.CharField(max_length=100)
    model = models.CharField(max_length=100)
    rent_cost = models.IntegerField()
    added_by = models.ForeignKey(User, on_delete=models.CASCADE)
    is_available = models.BooleanField(default=True)


class Order(models.Model):
    client_id = models.ForeignKey(Client, on_delete=models.CASCADE)
    car_id = models.ForeignKey(Car, on_delete=models.CASCADE)
    timestamp = models.DateTimeField(auto_now_add=True)
    from_date = models.DateTimeField()
    to_date = models.DateTimeField()
    added_by = models.ForeignKey(User, on_delete=models.CASCADE)


class Extended_order(models.Model):
    order = models.ForeignKey(Order, on_delete=models.CASCADE, related_name='extensions')
    from_datetime = models.DateTimeField()
    to_datetime = models.DateTimeField()

