from django.db import transaction
from rest_framework import status
from rest_framework.exceptions import ValidationError
from rest_framework.generics import (
    ListCreateAPIView,
    RetrieveUpdateDestroyAPIView,
    get_object_or_404,
    RetrieveDestroyAPIView
)
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from .serializers import (
    ClientSerializer,
    CarSerializer,
    OrderSerializer,
    ExtendedOrderSerializer,
    UpdateExtendedOrderSerializer,
    OrdersCountSerializer,
)
from .models import Client, Car, Order, Extended_order


class ClientView(ListCreateAPIView):
    serializer_class = ClientSerializer
    permission_classes = (IsAuthenticated,)

    def perform_create(self, serializer):
        return serializer.save(added_by=self.request.user)

    def get_queryset(self):
        return Client.objects.filter(added_by=self.request.user)


class ClientDetailView(RetrieveUpdateDestroyAPIView):
    serializer_class = ClientSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        return Client.objects.filter(added_by=self.request.user)


class CarView(ListCreateAPIView):
    serializer_class = CarSerializer
    permission_classes = (IsAuthenticated,)

    def perform_create(self, serializer):
        return serializer.save(added_by=self.request.user)

    def get_queryset(self):
        return Car.objects.filter(added_by=self.request.user)


class CarDetailView(RetrieveUpdateDestroyAPIView):
    serializer_class = CarSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        return Car.objects.filter(added_by=self.request.user)



class OrderView(ListCreateAPIView):
    serializer_class = OrderSerializer
    permission_classes = (IsAuthenticated,)

    def perform_create(self, serializer):
        with transaction.atomic():
            car = Car.objects.get(pk=self.request.data['car_id'])
            if car.is_available:
                car.is_available = False
                car.save()
                return serializer.save(added_by=self.request.user)
            else:
                raise ValidationError("this car is not available")

    def get_queryset(self):
        return Order.objects.filter(added_by=self.request.user)


class OrderDetailView(APIView):
    permission_classes = (IsAuthenticated,)
    http_method_names = ('get', 'put', 'delete')

    def get(self, request, pk):
        return Response(
            data=OrderSerializer(get_object_or_404(Order, pk=pk, added_by=request.user.id)).data
        )

    def put(self, request, pk):
        order = get_object_or_404(Order, pk=pk, added_by=request.user.id)
        serializer = OrderSerializer(order, request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        with transaction.atomic():
            car = order.car_id
            if car.id != request.data['car_id']:
                car.is_available = True
                car.save()
                car = Car.objects.get(pk=request.data['car_id'])
                if not car.is_available:
                    raise ValidationError("this car is not available")
                car.is_available = False
                car.save()
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

    def delete(self, request, pk):
        get_object_or_404(Order, pk=pk, added_by=request.user.id).delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class ExtendOrderView(ListCreateAPIView):
    serializer_class = ExtendedOrderSerializer
    permission_classes = (IsAuthenticated,)

    def perform_create(self, serializer):
        order = get_object_or_404(Order, pk=self.request.data['order'])
        last_extension = order.extensions.last()
        if not last_extension:
            return serializer.save(from_datetime=order.to_date)
        return serializer.save(from_datetime=last_extension.to_datetime)

    def get_queryset(self):
        return Extended_order.objects.filter(order__added_by=self.request.user)


class ExtendOrderDetailView(RetrieveDestroyAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = ExtendedOrderSerializer

    def get_queryset(self):
        return Extended_order.objects.filter(order__added_by=self.request.user)

    def put(self, request, pk):
        obj = get_object_or_404(Extended_order, pk=pk, order__added_by=self.request.user)
        if obj.order.extensions.last().id != pk:
            raise ValidationError('you can not change this')
        serializer = UpdateExtendedOrderSerializer(obj, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(data=serializer.data, status=status.HTTP_200_OK)


class OrdersCountView(APIView):
    http_method_names = ('post')
    permission_classes = (IsAuthenticated,)

    def post(self, request):
        serializer = OrdersCountSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        return Response(
            data=Order.objects.filter(
                added_by=request.user,
                timestamp__gte=serializer.validated_data['datetime']).count())


def profit_statistics(orders):
    profit = 0
    for order in orders:
        extensions = order.extensions
        if extensions.exists():
            period = extensions.last().to_datetime - order.from_date
        else:
            period = order.to_date - order.from_date
        profit += period.days * order.car_id.rent_cost
    return profit


class CarStatisticsView(APIView):
    http_method_names = ('get'),
    permission_classes = (IsAuthenticated,)

    def get(self, request, pk):
        orders = Order.objects.filter(added_by=request.user, car_id=pk)
        return Response(data={"count": orders.count(), "profit": profit_statistics(orders)})


class OrderStatisticsView(APIView):
    permission_classes = (IsAuthenticated,)
    http_method_names = ('get',)

    def get(self, request, pk):
        return Response(
            data={"count": Extended_order.objects.filter(order=pk).count()})


class ClientStatisticsView(APIView):
    permission_classes = (IsAuthenticated,)
    http_method_names = ('get',)

    def get(self, request, client, order):
        orders = Order.objects.filter(added_by=request.user, client_id=client)
        try:
            order = orders.get(pk=order)
        except Order.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
        extensions = order.extensions
        if extensions.exists():
            period = extensions.last().to_datetime - order.from_date
        else:
            period = order.to_date - order.from_date
        profit = period.days * order.car_id.rent_cost
        return Response(data={
            "count": orders.count(),
            "total_paid_money": profit_statistics(orders),
            "money_paid_for_this_order": profit
        })