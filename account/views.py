from rest_framework import status
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework_simplejwt.views import TokenObtainPairView
from .serializers import LogInSerializer, UserSerializer


class LogInView(TokenObtainPairView):
    permission_classes = (AllowAny,)
    serializer_class = LogInSerializer


class UserView(APIView):
    permission_classes = (IsAuthenticated,)
    http_method_names = ('get', 'put', 'delete')

    def get(self, request):
        return Response(UserSerializer(self.request.user).data)

    def put(self, request):
        serializer = UserSerializer(request.user, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_200_OK)

    def delete(self, request):
        request.user.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)