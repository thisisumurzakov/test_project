from rest_framework import serializers
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from django.contrib.auth.models import User


class LogInSerializer(TokenObtainPairSerializer):
    def validate(self, attrs):
        attrs = super(LogInSerializer, self).validate(attrs)
        attrs.update({'id': self.user.id})
        return attrs

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'first_name', 'last_name', 'email')