from django.urls import path, include
from .views import LogInView, UserView

urlpatterns = [
    path('user/', UserView.as_view()),
    path('login/', LogInView.as_view()),
]